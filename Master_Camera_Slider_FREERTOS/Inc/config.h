#ifndef CONFIG_H_
#define CONFIG_H_

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

void SystemClock_Config(void);
void MX_GPIO_Init(void);
void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);
void MX_I2C1_Init(void);
void MX_TIM1_Init(void);
void configInit	(void);

ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
TIM_HandleTypeDef htim1;

/* Private variables ---------------------------------------------------------*/
uint16_t adcVal;
uint8_t dir,speed,slejv,connection, IT_dir, IT_speed, blockL, blockR, debounce, debounce_enc;
char speed_str[10];

typedef struct{
	BaseType_t xReturned;
	TaskHandle_t xHandle;
}Task;

#endif
