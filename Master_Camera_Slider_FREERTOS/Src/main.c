
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "config.h"
#include "ssd1306.h"
#include "images.h"

void StartTask ( void );
void CreateStartTask( void );
void StepTask ( void );
void CreateStepTask( void );
void SpeedDirTask ( void );
void CreateSpeedDirTask( void );
void DebounceTask ( void );
void CreateDebounceTask( void );
void DebounceEncTask ( void );
void CreateDebounceEncTask( void );
void OLEDTask ( void );
void CreateOLEDTask( void );

void set_speed(uint8_t);
void make_step(void);
void set_direction(uint8_t);

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* Initialize all configured peripherals */
  configInit();
  CreateStartTask();
  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  while (1){}

}

void StartTask (){
	CreateStepTask();
	CreateSpeedDirTask();
	CreateDebounceTask();
	CreateDebounceEncTask();
	CreateOLEDTask();
	set_speed(0);
	while(1){
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		vTaskDelay(1000);
	}
}

void CreateStartTask( void )
{
	Task Start = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	Start.xReturned = xTaskCreate(
    				(TaskFunction_t) StartTask,       /* Function that implements the task. */
                    "Start",          /* Text name for the task. */
					configMINIMAL_STACK_SIZE,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &Start.xHandle );      /* Used to pass out the created task's handle. */

    if( Start.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( Start.xHandle );
}

void StepTask (){
	while(1){
		make_step();
	}
}

void CreateStepTask( void )
{
	Task Step = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	Step.xReturned = xTaskCreate(
    				(TaskFunction_t) StepTask,       /* Function that implements the task. */
                    "Step",          /* Text name for the task. */
					configMINIMAL_STACK_SIZE,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &Step.xHandle );      /* Used to pass out the created task's handle. */

    if( Step.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( Step.xHandle );
}

void SpeedDirTask (){
	while(1){
		if(((blockL && dir) || (blockR&& !dir)) && speed ){
		  speed = 0;
		  IT_speed = 0;
		  set_speed(0);
		}
		if(!connection){
		  if (IT_speed != speed && !blockL && !blockR){
			  speed = IT_speed;
			  set_speed(speed);
		  }
		  if(IT_dir != dir){
			  if(blockL || blockR){
				set_speed(speed);
				blockL = 0;
				blockR = 0;
			  }
			  dir = IT_dir;
			  set_direction(dir);
		  }
		}
		else if (slejv){
		  dir = slejv >> 7;
		  set_direction(dir);
		  speed = slejv&7;
		  if(blockL && !dir)
			  blockL = 0;
		  else if(blockR && dir)
			  blockR = 0;
		  if(!blockL && !blockR)
			  set_speed(speed);
		  slejv = 0;
		}
	}
}

void CreateSpeedDirTask( void )
{
	Task SpeedDir = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	SpeedDir.xReturned = xTaskCreate(
    				(TaskFunction_t) SpeedDirTask,       /* Function that implements the task. */
                    "SpeedDir",          /* Text name for the task. */
					configMINIMAL_STACK_SIZE,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &SpeedDir.xHandle );      /* Used to pass out the created task's handle. */

    if( SpeedDir.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( SpeedDir.xHandle );
}

void DebounceTask (){
	while(1){
		if(debounce){
		  vTaskDelay(300);
		  debounce = 0;
		}
	}
}

void CreateDebounceTask( void )
{
	Task Debounce = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	Debounce.xReturned = xTaskCreate(
    				(TaskFunction_t) DebounceTask,       /* Function that implements the task. */
                    "Debounce",          /* Text name for the task. */
					configMINIMAL_STACK_SIZE,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &Debounce.xHandle );      /* Used to pass out the created task's handle. */

    if( Debounce.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( Debounce.xHandle );
}

void DebounceEncTask (){
	while(1){
		if(debounce_enc){
		  vTaskDelay(200);
		  debounce_enc = 0;
		}
	}
}

void CreateDebounceEncTask( void )
{
	Task DebounceEnc = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	DebounceEnc.xReturned = xTaskCreate(
    				(TaskFunction_t) DebounceEncTask,       /* Function that implements the task. */
                    "DebounceEnc",          /* Text name for the task. */
					configMINIMAL_STACK_SIZE,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &DebounceEnc.xHandle );      /* Used to pass out the created task's handle. */

    if( DebounceEnc.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( DebounceEnc.xHandle );
}

void OLEDTask (){
	while(1){
		speed_str[0] = speed +'0';
		ssd1306_SetCursor(80, 2);
		ssd1306_WriteString(speed_str, Font_11x18, White);
		draw_arrow(dir,100,2);
		draw_controller(connection,15,24);
		ssd1306_UpdateScreen();
		vTaskDelay(10);
	}
}

void CreateOLEDTask( void )
{
	ssd1306_Init();
	ssd1306_Fill(Black);
	ssd1306_SetCursor(2, 2);
	ssd1306_WriteString("SPEED:", Font_11x18, White);

	Task OLED = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	OLED.xReturned = xTaskCreate(
    				(TaskFunction_t) OLEDTask,       /* Function that implements the task. */
                    "OLED",          /* Text name for the task. */
					configMINIMAL_STACK_SIZE,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &OLED.xHandle );      /* Used to pass out the created task's handle. */

    if( OLED.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( OLED.xHandle );
}

void set_speed(uint8_t speed){
	switch(speed){
	case 0:
		HAL_GPIO_WritePin(BED_EN_GPIO_Port, BED_EN_Pin, GPIO_PIN_SET);
		break;
	case 1:
		HAL_GPIO_WritePin(BED_MS1_GPIO_Port, BED_MS1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(BED_MS2_GPIO_Port, BED_MS2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(BED_MS3_GPIO_Port, BED_MS3_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(BED_EN_GPIO_Port, BED_EN_Pin, GPIO_PIN_RESET);
		break;
	case 2:
		HAL_GPIO_WritePin(BED_MS1_GPIO_Port, BED_MS1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(BED_MS2_GPIO_Port, BED_MS2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(BED_MS3_GPIO_Port, BED_MS3_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(BED_EN_GPIO_Port, BED_EN_Pin, GPIO_PIN_RESET);
		break;
	case 3:
		HAL_GPIO_WritePin(BED_MS1_GPIO_Port, BED_MS1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(BED_MS2_GPIO_Port, BED_MS2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(BED_MS3_GPIO_Port, BED_MS3_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(BED_EN_GPIO_Port, BED_EN_Pin, GPIO_PIN_RESET);
		break;
	case 4:
		HAL_GPIO_WritePin(BED_MS1_GPIO_Port, BED_MS1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(BED_MS2_GPIO_Port, BED_MS2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(BED_MS3_GPIO_Port, BED_MS3_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(BED_EN_GPIO_Port, BED_EN_Pin, GPIO_PIN_RESET);
		break;
	}
	return;
}

void make_step(){
	HAL_GPIO_WritePin(BED_Step_GPIO_Port, BED_Step_Pin, GPIO_PIN_SET);
	vTaskDelay(2);
	HAL_GPIO_WritePin(BED_Step_GPIO_Port, BED_Step_Pin, GPIO_PIN_RESET);
	vTaskDelay(2);
	return;
}

void set_direction(uint8_t dir){
	if(dir)
		HAL_GPIO_WritePin(BED_Dir_GPIO_Port, BED_Dir_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(BED_Dir_GPIO_Port, BED_Dir_Pin, GPIO_PIN_RESET);
	return;
}



/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM4 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == TIM4)
    HAL_IncTick();
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
