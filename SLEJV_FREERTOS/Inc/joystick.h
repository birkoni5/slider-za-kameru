#include "stm32f1xx_hal.h"
#include "config.h"

uint32_t 	getVRx	(void);
uint32_t 	getVRy	(void);
uint8_t 	getSW	(void);
