#ifndef CONFIG_H_
#define CONFIG_H_

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

void 	SystemClock_Config	(void);
void 	MX_GPIO_Init		(void);
void 	SystemClock_Config	(void);
void 	MX_USART1_UART_Init	(void);
void 	MX_ADC1_Init		(void);
void 	MX_ADC2_Init		(void);
void 	configInit			(void);
void 	MX_I2C1_Init		(void);

#define LOC_TRESHOLD 	1500 //0-2047
#define JOYSW_PIN 		GPIO_PIN_2
#define JOYSW_PORT 		GPIOA

typedef struct{
	BaseType_t xReturned;
	TaskHandle_t xHandle;
}Task;

ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;
I2C_HandleTypeDef hi2c1;
UART_HandleTypeDef huart1;

#endif
