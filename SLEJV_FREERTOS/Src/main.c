/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "config.h"
#include "joystick.h"
#include "ssd1306.h"
#include "images.h"

/* Private variables ---------------------------------------------------------*/
uint32_t adcX, adcY;
uint8_t speed,dir,btn,prev_speed,prev_dir, package;
char speed_str[10];

/* Private function prototypes -----------------------------------------------*/
void StartTask ( void );
void CreateStartTask( void );
void JoystickXTask( void );
void CreateJoystickXTask( void );
void JoystickYTask( void );
void CreateJoystickYTask( void );
void BtnTask( void );
void CreateBtnTask( void );
void UARTTask( void );
void CreateUARTTask( void );
void OLEDTask ( void );
void CreateOLEDTask( void );


int main(void)
{
	configInit();
	CreateStartTask();
	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	while (1){}
}


void StartTask (){
	CreateJoystickXTask();
	CreateJoystickYTask();
	CreateBtnTask();
	CreateUARTTask();
	CreateOLEDTask();
	while(1){
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		vTaskDelay(1000);
	}
}

void CreateStartTask( void )
{
	Task Start = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	Start.xReturned = xTaskCreate(
    				(TaskFunction_t) StartTask,       /* Function that implements the task. */
                    "Start",          /* Text name for the task. */
					64,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &Start.xHandle );      /* Used to pass out the created task's handle. */

    if( Start.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( Start.xHandle );
}

void JoystickXTask()
{
    for( ;; ){
		adcX = getVRx();
		if(adcX>4095-LOC_TRESHOLD){
			if(speed<4){
				speed++;
				vTaskDelay(1000);
			}
		}
		else if(adcX<LOC_TRESHOLD)
			if(speed>0){
				speed--;
				vTaskDelay(1000);
			}
    	vTaskDelay(10);
    }
}

void CreateJoystickXTask( void )
{
	Task JoystickX = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	JoystickX.xReturned = xTaskCreate(
    				(TaskFunction_t) JoystickXTask,       /* Function that implements the task. */
                    "JoystickX",          /* Text name for the task. */
					64,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &JoystickX.xHandle );      /* Used to pass out the created task's handle. */

    if( JoystickX.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( JoystickX.xHandle );
}

void JoystickYTask()
{
    for( ;; ){
    	adcY = getVRy();
		if(adcY>4095-LOC_TRESHOLD){
			dir=1;
			vTaskDelay(1000);
		}
		else if(adcY<LOC_TRESHOLD){
			dir=0;
			vTaskDelay(1000);
		}
		vTaskDelay(10);
    }
}

void CreateJoystickYTask( void )
{
	Task JoystickY = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	JoystickY.xReturned = xTaskCreate(
    				(TaskFunction_t) JoystickYTask,       /* Function that implements the task. */
                    "JoystickY",          /* Text name for the task. */
					64,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &JoystickY.xHandle );      /* Used to pass out the created task's handle. */

    if( JoystickY.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( JoystickY.xHandle );
}

void BtnTask( void)
{
    for( ;; ){
    	btn = getSW();
    	if(!btn){
    		speed = 0;
    		vTaskDelay(300);
    	}
    	vTaskDelay(10);
    }
}

void CreateBtnTask( void )
{
	Task Btn = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	Btn.xReturned = xTaskCreate(
    				(TaskFunction_t) BtnTask,       /* Function that implements the task. */
                    "Btn",          /* Text name for the task. */
					64,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &Btn.xHandle );      /* Used to pass out the created task's handle. */

    if( Btn.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( Btn.xHandle );
}
void UARTTask( void)
{
    for( ;; ){
    	if(speed!=prev_speed || dir!=prev_dir){
    		package = dir<<7;
    		package += 8; // ignorirao je slanje 8 nula, pa smo postavili jedan bit koji ne koristimo u jedinicu
    		package |= speed;
    		prev_dir = dir;
    		prev_speed = speed;
    		HAL_UART_Transmit(&huart1, &package,1, 0xFFFF);
    	}
    	vTaskDelay(10);
    }
}

void CreateUARTTask( void )
{
	Task UART = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	UART.xReturned = xTaskCreate(
    				(TaskFunction_t) UARTTask,       /* Function that implements the task. */
                    "UART",          /* Text name for the task. */
					64,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &UART.xHandle );      /* Used to pass out the created task's handle. */

    if( UART.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( UART.xHandle );
}

void OLEDTask (){
	while(1){
		speed_str[0] = speed +'0';
		ssd1306_SetCursor(80, 2);
		ssd1306_WriteString(speed_str, Font_11x18, White);
		draw_arrow(dir,100,2);
		draw_controller(1,15,24);
		ssd1306_UpdateScreen();
		vTaskDelay(10);
	}
}

void CreateOLEDTask( void )
{

	ssd1306_Init();
	ssd1306_Fill(Black);
	ssd1306_SetCursor(2, 2);
	ssd1306_WriteString("SPEED:", Font_11x18, White);

	Task OLED = {.xHandle = NULL};

    /* Create the task, storing the handle. */
	OLED.xReturned = xTaskCreate(
    				(TaskFunction_t) OLEDTask,       /* Function that implements the task. */
                    "OLED",          /* Text name for the task. */
					configMINIMAL_STACK_SIZE,      /* Stack size in words, not bytes. */
                    (void*)0,    /* Parameter passed into the task. */
                    5,/* Priority at which the task is created. */
                    &OLED.xHandle );      /* Used to pass out the created task's handle. */

    if( OLED.xReturned == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY )
        vTaskDelete( OLED.xHandle );
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == TIM2) {
    HAL_IncTick();
  }
}
void _Error_Handler(char *file, int line)
{
  /* User can add his own implementation to report the HAL error return state */
  while(1){}
}
