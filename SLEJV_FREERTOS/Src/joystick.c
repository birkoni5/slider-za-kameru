#include "joystick.h"

uint32_t getVRx(){
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 100);
	uint32_t adcX = HAL_ADC_GetValue(&hadc1);
	HAL_ADC_Stop(&hadc1);
	return adcX;
}

uint32_t getVRy(){
	HAL_ADC_Start(&hadc2);
	HAL_ADC_PollForConversion(&hadc2, 100);
	uint32_t adcY = HAL_ADC_GetValue(&hadc2);
	HAL_ADC_Stop(&hadc2);
	return adcY;
}

uint8_t getSW(){
	return HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2);
}
